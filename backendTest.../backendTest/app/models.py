from django.db import models

# Create your models here.
class listingsAndReviews(models.Model):
    
    # _id = models.CharField(max_length=120, default="")

    listing_url = models.CharField(max_length=120, default="")
    name = models.CharField(max_length=120, default="")
    summary = models.CharField(max_length=120, default="")
    space= models.CharField(max_length=120, default="")
    description=models.CharField(max_length=120, default="")
    neighborhood_overview=models.CharField(max_length=120, default="")
    notes=models.CharField(max_length=120, default="")
    transit=models.CharField(max_length=120, default="")
    access=models.CharField(max_length=120, default="")
    interaction=models.CharField(max_length=120, default="")
    house_rules=models.CharField(max_length=120, default="")
    property_type=models.CharField(max_length=120, default="")
    room_type=models.CharField(max_length=120, default="")
    bed_type=models.CharField(max_length=120, default="")
    minimum_nights=models.CharField(max_length=120, default="")
    maximum_nights=models.CharField(max_length=120, default="")
    cancellation_policy=models.CharField(max_length=120, default="")
    last_scraped = models.DateTimeField()
    calendar_last_scraped = models.DateTimeField()
    accommodates=models.IntegerField(default=0)
    bedrooms=models.IntegerField(default=0)
    beds=models.IntegerField(default=0)
    number_of_reviews=models.IntegerField(default=0)
    bathrooms=models.FloatField(default=0.00)
    amenities=models.JSONField(default=[])
    price=models.FloatField(default=0.00)
    extra_people=models.FloatField(default=0.00)
    guests_included=models.FloatField(default=0.00)
    images=models.JSONField(default={})
    host=models.JSONField(default={})
    address=models.JSONField(default={})
    availability=models.JSONField(default={})
    review_scores=models.JSONField(default={})
    reviews=models.JSONField(default=[])


    class Meta:
        db_table = 'listingsAndReviews'

